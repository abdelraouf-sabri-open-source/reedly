# Reedly The RSS Reader

<!-- 
Generate TOC using this commands :
1. `wget https://raw.githubusercontent.com/ekalinin/github-markdown-toc/master/gh-md-toc`
1. `chmod a+x gh-md-toc`
1. `mv gh-md-toc ~/.local/bin/`
1. From Android studio open a terminal and run `gh-md-toc src README.md`
-->

## Table of Contents

   * [Reedly The RSS Reader](README.md#reedly-the-rss-reader)
      * [Table of Contents](README.md#table-of-contents)
      * [Features of this repo](README.md#features-of-this-repo)
      * [Installation](README.md#installation)
         * [Prerequisites](README.md#prerequisites)
         * [Steps](README.md#steps)

## Features of this repo
 
 - Clean Architecture
 - Test Driven Development 
 - Acceptance testing using `FitNesse` **incoming-feature**
 - Dependency injection using `Dagger2` **incoming-feature**
 
 And many more would be listed here
 
## Installation
 
### Prerequisites
 1. Android studio `2.3.3+`
 1. JDK `1.8.xx`
 1. Build Tools Version `25.0.3`
 1. Installed SDKs `16 .. 25`
 1. Support Lib Version `25.3.1`

### Steps
 1. `git clone https://gitlab.com/abdelraouf-sabri-open-source/reedly.git`
 1. Import project into Android studio